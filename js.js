var values = [];
let color;
var counter = 0;

/**
 * Create a new image and return it
 * @param canvas
 * @returns {HTMLImageElement}
 */

function getImage(canvas){
    var imageData = canvas.toDataURL();
    var image = new Image();
    image.src = imageData;
    return image;
}

/**
 * Create a link to download image and set attribute to get it
 * @param image
 */

function saveImage(image) {
    var link = document.createElement("a");

    link.setAttribute("href", image.src);
    link.setAttribute("download", "canvasImage");
    link.click();
}

/**
 * Get canvas with id
 */

function saveCanvasAsImageFile(){
    var image = getImage(document.getElementById("canvas"));
    saveImage(image);
}

/**
 * Request to file input.txt, get all necessary data, save this data in variable values
 */

(function press() {
    let arr = [];
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', 'input.txt', false);
    xmlhttp.send(null);

    if (xmlhttp.status === 200) {
        var response = xmlhttp.responseText;
        let str = '';
        for (let i = 0; i < response.length; i++) {
            if (response[i] === ' ') {
                arr.push(i);
            }
        }
        
        for (let i = 0; i < arr.length; i++) {
            if (i === arr.length - 1) {
                for (let j = arr[i] + 1; j <= response.length - 1; j++) {
                    str += response[j];
                }
            }

            for (let j = arr[i] + 1; j <= arr[i+1] - 1; j++) {
                str += response[j];
            }
            values.push(str);
            str = '';
        }
    }

    if(values.length > 11 || values.length < 11) throw('Wrong values!! Check input.txt');
    color = values[values.length - 1];
    for (let i = 0; i < values.length - 1; i++) {
        if (isNaN(parseInt(values[i])) === true) throw ('Wrong values!! Check input.txt');
    }
    for(let i in values) {
        values[i] = parseInt(values[i]);
    }
})();

/**
 * Draw lines on the necessary coordinates
 * @param props
 */

function line(props) {
    const {ctx,x,y} = props;
    ctx.font = 'bold 16px sans-serif';
    ctx.fillStyle = '#000000';
    ctx.fillText('x',x,y);
}

/**
 * General component. Draw all lines and fill area
 */

class CanvasComponent extends React.Component {

    /**
     * Check conditions to unlock button Fill
     * @param q
     */

    checkToUnlock(q) {
        counter += q;
        if (counter % 2 !== 0 && counter !== 1) {
            console.log("gello");
            let elem = document.querySelector(".fill-btn");
            elem.removeAttribute("disabled");
            elem.onclick = () => {
                let ctx = this.refs.canvas.getContext('2d');
                this.floodFill(ctx);
            }
        }
    }

    /**
     * Draw lines from x1 y1 to x2 y2
     * @param ctx
     */

    drawLines(ctx) {
        let y = values[3];

        for (var x = values[2]; x <= values[4]; x+=10) {
            line({ctx,x,y});
        }

        for (y; y <= values[5]; y+=10) {
            line({ctx,x,y});
        }
        let q = 1;
        this.checkToUnlock(q);
    }

    /**
     * Draw rectangle from x1 y1 to x2 y2
     * @param ctx
     */

    drawRectangle(ctx) {
        let x = values[6];
        let y = values[7];
        let x2 = values[8];
        let y2 = values[9];

        for(x; x <= x2; x+=10) {
            line({ctx,x,y});
        }

        for(y; y <= y2; y+=10) {
            line({ctx,x,y});
        }
        x = values[6];
        y = values[7] + 10;

        for(y; y <= y2; y+=10) {
            line({ctx,x,y});
        }

        for(x; x <= x2 + 10; x+=10) {
            line({ctx,x,y});
        }
        let w = 2;
        this.checkToUnlock(w);
    }

    /**
     * Make the area fill the necessary color
     * @param ctx
     */

    floodFill(ctx) {
        ctx.beginPath();
        ctx.rect(0, 0, values[0], values[1]);
        switch (color) {
            case 'o':
                ctx.fillStyle = "orange";
                break;
            case 'b':
                ctx.fillStyle = "blue";
                break;
            case 'g':
                ctx.fillStyle = "grey";
                break;
            case 'y':
                ctx.fillStyle = "yellow";
                break;
            case 'l':
                ctx.fillStyle = "green";
                break;
            case 'r':
                ctx.fillStyle = "red";
                break;
            default:
                throw 'Wrong color!';
        }
        ctx.fill();
        let k;
        (values[8]-values[6]) % 10 === 0 ? k = 20 : k = 15;
        ctx.beginPath();
        ctx.rect(values[6],values[7] - k/2,(values[8]-values[6]) + k,(values[9]-values[7]) + k);
        ctx.fillStyle = 'white';
        ctx.fill();

        if (values[2] === 0 && values[5] === values[1]) {
            ctx.rect(values[2],values[3] - k/2 ,values[4] + k,(values[5] - values[3]) + k/2);
            ctx.fillStyle = "white";
            ctx.fill();
        }
        this.drawLines(ctx);
        this.drawRectangle(ctx);
    }
    render() {
        return (
            <React.Fragment>
                <canvas id="canvas" ref="canvas" width={values[0]} height={values[1]}/>
                <button onClick={() => {
                    let ctx = this.refs.canvas.getContext('2d');
                    this.drawLines(ctx);
                }}>Draw lines</button>
                <button onClick={() => {
                    let ctx = this.refs.canvas.getContext('2d');
                    this.drawRectangle(ctx);
                }}>Draw rectangle</button>
                <button onClick={saveCanvasAsImageFile}>Download</button>
                <button disabled={true} className='fill-btn' onClick={() => {
                    let ctx = this.refs.canvas.getContext('2d');
                    this.floodFill(ctx);
                }}>Fill</button>
            </React.Fragment>
        );
    }
}

ReactDOM.render(<CanvasComponent/>, document.getElementById('container'));
